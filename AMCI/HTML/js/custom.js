jQuery(function () {
    'use strict';

    var lastpos,
        $header = $('header');

    jQuery(window).scroll(function () {
        var winpos = $(window).scrollTop();

        if (winpos < lastpos) {
            $header.addClass('moveon');
            $header.removeClass('moveout');
        } else {
            $header.addClass('moveout');
            $header.removeClass('moveon');
        }

        if (winpos < $header.height()) {
            if ($header.hasClass('moveon'))
                $header.addClass('moveon');
        }
        lastpos = $(document).scrollTop();

    });
});

jQuery(document).ready(function () {
     jQuery (".course .nav-tabs .nav-item .nav-link"). click (function () {
        var link = jQuery(this).attr('href').replace('#', '');
        jQuery (".course .nav-tabs"). attr ('class', 'nav nav-tabs ' + link);
    });
    var owl = $('.owl-carousel.blog');
    owl.owlCarousel({
        loop: true,
        nav: true,
        margin: 10,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            960: {
                items: 2
            },
            1200: {
                items: 2
            }
        }
    });
    owl.on('mousewheel', '.owl-stage', function (e) {
        if (e.deltaY > 0) {
            owl.trigger('next.owl');
        } else {
            owl.trigger('prev.owl');
        }
        e.preventDefault();
    });
})
jQuery('.firstCap').on('keypress', function (event) {
    var $this = $(this),
        thisVal = $this.val(),
        FLC = thisVal.slice(0, 1).toUpperCase();
    con = thisVal.slice(1, thisVal.length);
    jQuery(this).val(FLC + con);
});




